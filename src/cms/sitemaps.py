from django.contrib.sitemaps import Sitemap

from .models import Piece


class CatalogSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Piece.objects.filter(publish=True)

    def lastmod(self, obj):
        return obj.date_updated

from decimal import Decimal

from adminsortable.fields import SortableForeignKey
from adminsortable.models import SortableMixin
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.urls import reverse
from solo.models import SingletonModel
from uuslug import uuslug

IMAGE_SIZE_BIG_W = 2000
IMAGE_SIZE_BIG_H = 1000

IMAGE_SIZE_SMALL_W = 1000
IMAGE_SIZE_SMALL_H = 500

PIECE_STATUS_OPTIONS = (("stck", "En Stock"), ("sold", "Agotado"))
WORKSHOP_STATUS_OPTIONS = (("opn", "Open"), ("fll", "Full"), ("exp", "Expired"))
PERCENTAGE_VALIDATOR = [MinValueValidator(0), MaxValueValidator(100)]


def upload_to(instance, filename):
    import os

    from django.utils.timezone import now

    base, ext = os.path.splitext(filename)
    time = now().strftime("%Y%m%d%H%M%S")
    return f"uploads/{base}{time}{ext.lower()}"


class Site(SingletonModel):
    title = models.CharField(max_length=140, default="Gama Estudio")
    social_description = models.CharField(max_length=140, default="Social description")
    preview_image = models.ImageField(upload_to=upload_to)

    email = models.EmailField(max_length=140)
    instagram = models.URLField(max_length=140, default="https://instagram.com/")
    facebook = models.URLField(max_length=140, default="https://www.facebook.com/")

    contact_success_message = models.CharField(
        max_length=140, default="¡Gracias por contactarnos!"
    )
    contact_text = models.TextField("Texto de contacto", null=True, blank=True)

    def __str__(self):
        return "Textos del sitio"

    class Meta:
        verbose_name = "Textos del sitio"
        verbose_name_plural = "Textos del sitio"


class PieceCategory(SortableMixin):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    name = models.CharField(max_length=140, default="Nombre")
    slug = models.CharField(max_length=240, blank=True, null=True, editable=False)
    width = models.PositiveIntegerField(default=0, editable=False)
    height = models.PositiveIntegerField(default=0, editable=False)
    image = models.ImageField(
        "Imagen",
        upload_to=upload_to,
        width_field="width",
        height_field="height",
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["order"]
        verbose_name = "Categorgía de piezas"
        verbose_name_plural = "Categorías de piezas"

    @property
    def random_published_piece(self):
        return Piece.objects.filter(category=self, publish=True).order_by("?").first()


class Piece(SortableMixin):
    publish = models.BooleanField(default=False)
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    name = models.CharField(max_length=140, default="Nombre")
    slug = models.CharField(max_length=240, blank=True, null=True, editable=False)
    category = SortableForeignKey(
        "PieceCategory", on_delete=models.CASCADE, related_name="pieces"
    )
    status = models.CharField(
        "Status",
        max_length=4,
        default=PIECE_STATUS_OPTIONS[0],
        choices=PIECE_STATUS_OPTIONS,
    )
    price = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    discount_perc = models.DecimalField(
        "Price discount",
        max_digits=3,
        decimal_places=0,
        default=Decimal(0),
        validators=PERCENTAGE_VALIDATOR,
    )
    weight = models.FloatField("peso (g)", default=0)
    diameter = models.FloatField("diámetro (cm)", default=0)
    length = models.FloatField("largo (cm)", default=0)
    width = models.FloatField("ancho (cm)", default=0)
    height = models.FloatField("alto (cm)", default=0)
    summary = models.TextField("Pequeña descripción", null=True, blank=True)
    description = models.TextField("Descripción", null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    @property
    def price_discounted(self):
        if self.discount_perc == 0:
            return self.price
        return self.price - (self.price * (self.discount_perc / 100))

    def all_images(self):
        return PieceImage.objects.filter(piece=self, publish=True)

    def admin_image(self):
        return PieceImage.objects.filter(piece=self).first()

    def main_image(self):
        all_published_images = PieceImage.objects.filter(piece=self, publish=True)

        home_image = all_published_images.filter(show_on_home=True).first()

        if home_image:
            return home_image

        return all_published_images.first()

    @property
    def image(self):
        return PieceImage.objects.filter(piece=self, publish=True).first()

    def get_absolute_url(self):
        return reverse(
            "catalog_detail",
            kwargs={"category_slug": self.category.slug, "slug": self.slug},
        )

    @property
    def external_shop(self):
        if not self.externalshoppiece_set.exists():
            return None

        return self.externalshoppiece_set.first().external_shop

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["order"]
        verbose_name = "Pieza"
        verbose_name_plural = "Piezas"


class PieceImage(SortableMixin):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    publish = models.BooleanField(default=True)
    show_on_home = models.BooleanField(default=False)
    width = models.PositiveIntegerField(default=0, editable=False)
    height = models.PositiveIntegerField(default=0, editable=False)
    piece = SortableForeignKey("Piece", on_delete=models.CASCADE, related_name="images")
    image = models.ImageField(
        "Imagen", upload_to=upload_to, width_field="width", height_field="height"
    )

    def __str__(self):
        return self.image.url

    class Meta:
        ordering = ["order"]
        verbose_name = "Imagen de Pieza"
        verbose_name_plural = "Imagenes de piezas"


class Us(SingletonModel):
    title = models.CharField(max_length=140, default="Nosotras")
    image = models.ImageField("Imagen", upload_to=upload_to)
    text = models.TextField("Contenido")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Us"
        verbose_name_plural = "Us"


class Project(SortableMixin):
    publish = models.BooleanField(default=False)
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    name = models.CharField(max_length=140, default="Nombre")
    slug = models.CharField(max_length=240, blank=True, null=True, editable=False)
    description = models.CharField(max_length=140, default="descripción")
    link = models.URLField(max_length=140, default="https://gama-estudio.com/")
    image = models.ImageField("Imagen", upload_to=upload_to)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["order"]
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyectos"


class HomeImage(SortableMixin):
    publish = models.BooleanField(default=False)
    name = models.CharField(max_length=140, default="Texto alterno")
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    width = models.PositiveIntegerField(default=0, editable=False)
    height = models.PositiveIntegerField(default=0, editable=False)
    link = models.URLField(max_length=140, default="https://gama-estudio.com/")
    image = models.ImageField(
        "Imagen", upload_to=upload_to, height_field="height", width_field="width"
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["order"]
        verbose_name = "Imágen del home"
        verbose_name_plural = "Imágenes del home"


class Workshop(SortableMixin):
    publish = models.BooleanField(default=False)
    title = models.CharField(max_length=240)
    price = models.FloatField(default=0)
    status = models.CharField(
        "Status",
        max_length=3,
        default=WORKSHOP_STATUS_OPTIONS[0][0],
        choices=WORKSHOP_STATUS_OPTIONS,
    )
    summary = models.TextField("Pequeña descripción", null=True, blank=True)
    description = models.TextField("Descripción", null=True, blank=True)
    slug = models.CharField(max_length=240, blank=True, null=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    def all_images(self):
        return WorkshopImage.objects.filter(workshop=self, publish=True)

    def admin_image(self):
        return WorkshopImage.objects.filter(workshop=self).first()

    def main_image(self):
        return WorkshopImage.objects.filter(workshop=self, publish=True).first()

    @property
    def image(self):
        return WorkshopImage.objects.filter(workshop=self, publish=True).first()

    def get_absolute_url(self):
        return reverse("workshop_detail", kwargs={"slug": self.slug})

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["order"]
        verbose_name = "Workshop"
        verbose_name_plural = "Workshops"


class WorkshopImage(SortableMixin):
    publish = models.BooleanField(default=False)
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    width = models.PositiveIntegerField(default=0, editable=False)
    height = models.PositiveIntegerField(default=0, editable=False)
    workshop = SortableForeignKey(
        "Workshop", on_delete=models.CASCADE, related_name="images"
    )
    image = models.ImageField("Imagen", upload_to=upload_to)

    def save(self, *args, **kwargs):
        self.width = self.image.width
        self.height = self.image.height
        super().save(*args, **kwargs)

    def __str__(self):
        return self.image.url

    class Meta:
        ordering = ["order"]
        verbose_name = "Imagen de Workshop"
        verbose_name_plural = "Imagenes de workshops"


class Shop(SingletonModel):
    checkout_text = models.TextField("Texto de checkout", null=True, blank=True)
    review_order_text = models.TextField(
        "Texto de revisar orden", null=True, blank=True
    )
    payment_done_open_text = models.TextField(
        "Primer Texto de orden pagada", null=True, blank=True
    )
    payment_done_close_text = models.TextField(
        "Segundo Texto de orden pagada", null=True, blank=True
    )
    payment_cancelled_text = models.TextField(
        "Texto de orden cancelada", null=True, blank=True
    )
    email_confirmation_opening_text = models.TextField(
        "Texto abre confirmación en email",
        null=True,
        blank=True,
        default="te dejamos los detalles de tu pedido:",
    )
    email_confirmation_closing_text = models.TextField(
        "Texto cierre confirmación en email",
        null=True,
        blank=True,
        default="Si tienes alguna pregunta, responde este correo electrónico, o contáctanos a través de tienda@gama-estudio.com",
    )

    def __str__(self):
        return "Textos de la tienda"

    class Meta:
        verbose_name = "Textos de la tienda"
        verbose_name_plural = "Textos de la tienda"


class FrequentQuestion(SortableMixin):
    publish = models.BooleanField(default=False)
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    question_text = models.TextField("Pregunta", default="¿?", null=False, blank=False)
    answer_text = models.TextField("Respuesta", null=False, blank=False)

    def __str__(self):
        return self.question_text

    class Meta:
        ordering = ["order"]
        verbose_name = "Pregunta frecuente"
        verbose_name_plural = "Preguntas Frecuentes"


class ExternalShop(models.Model):
    name = models.CharField(default="", max_length=240, null=True, blank=True)
    link = models.URLField(max_length=240)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Tienda externa"
        verbose_name_plural = "Tiendas externas"


class ExternalShopPiece(models.Model):
    external_shop = models.ForeignKey(
        ExternalShop, on_delete=models.CASCADE, blank=False, null=False
    )
    piece = models.ForeignKey(Piece, on_delete=models.CASCADE, blank=False, null=False)

    def __str__(self):
        return f"{self.external_shop.name}-{self.piece.name}"

    class Meta:
        verbose_name = "Tienda externa"
        verbose_name_plural = "Tiendas externas"

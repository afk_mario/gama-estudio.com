from django.contrib import messages
from django.core.mail import EmailMessage
from django.shortcuts import render
from django.template.loader import get_template
from django.urls import reverse
from django.views.generic import DetailView, FormView, ListView

from shop.context_processors import cart_info

from .forms import ContactForm
from .models import (
    FrequentQuestion,
    HomeImage,
    Piece,
    PieceCategory,
    Project,
    Site,
    Us,
    Workshop,
)

DEFAULT_TITLE = "Gama Estudio"
DEFAULT_DESCRIPTION = ""
DEFAULT_PREVIEW = ""


def home(request):
    site = Site.get_solo()
    i_list = HomeImage.objects.filter(publish=True).order_by("?")
    context = {
        "title": site.title,
        "description": site.social_description,
        "preview": site.preview_image.url,
        "i_list": i_list,
    }

    return render(request, "cms/home.html", context)


class CategoryList(ListView):
    model = PieceCategory

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        site = Site.get_solo()
        custom_context = {
            "title": site.title,
            "description": site.social_description,
            "preview": site.preview_image.url,
        }
        return {**context, **custom_context}


class PieceList(ListView):
    model = Piece
    paginate_by = 21

    def get_queryset(self):
        return (
            Piece.objects.filter(
                publish=True, category__slug=self.kwargs["category_slug"]
            )
            .prefetch_related("images")
            .order_by("-status", "name", "date_created")
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_list = PieceCategory.objects.all()
        site = Site.get_solo()
        custom_context = {
            "title": site.title,
            "description": site.social_description,
            "preview": site.preview_image.url,
            "category_slug": self.kwargs["category_slug"],
            "category_list": category_list,
        }
        return {**context, **custom_context}


class PieceDetails(DetailView):
    model = Piece

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        from_url = reverse(
            "catalog", kwargs={"category_slug": self.kwargs["category_slug"]}
        )

        is_in_cart = False

        order = cart_info(self.request).get("cart_order")

        if order:
            result = order.orderitem_set.all().filter(product__id=self.object.id)
            if result:
                is_in_cart = True

        if self.request.GET:
            from_url = self.request.GET.get("from", from_url)

        site = Site.get_solo()
        custom_context = {
            "title": f"{site.title} | {self.object.name}",
            "description": site.social_description,
            "preview": site.preview_image.url,
            "next": self.object.get_next(),
            "prev": self.object.get_previous(),
            "from_url": from_url,
            "is_in_cart": is_in_cart,
        }
        return {**context, **custom_context}


def us(request):
    site = Site.get_solo()
    single = Us.get_solo()
    context = {
        "title": site.title,
        "description": site.social_description,
        "preview": site.preview_image.url,
        "single": single,
    }

    return render(request, "cms/us.html", context)


def projects(request):
    i_list = Project.objects.filter(publish=True)
    site = Site.get_solo()
    context = {
        "i_list": i_list,
        "title": site.title,
        "description": site.social_description,
        "preview": site.preview_image.url,
    }

    return render(request, "cms/projects.html", context)


class Contact(FormView):
    template_name = "cms/contact.html"
    form_class = ContactForm

    def get_success_url(self):
        return reverse("contact")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        site = Site.get_solo()
        context["title"] = site.title
        context["description"] = (site.social_description,)
        context["preview"] = (site.preview_image.url,)
        return context

    def form_valid(self, form):
        site = Site.get_solo()
        contact_name = form.cleaned_data["contact_name"]
        contact_email = form.cleaned_data["contact_email"]
        contact_message = form.cleaned_data["contact_message"]

        # Email the profile with the
        # contact information
        template = get_template("cms/contact_template.txt")
        context = {
            "contact_name": contact_name,
            "contact_email": contact_email,
            "contact_message": contact_message,
        }

        content = template.render(context)
        send_to = site.email
        send_from = site.email

        email = EmailMessage(
            "Nuevo corre de contacto",
            content,
            send_from,
            [send_to],
            reply_to=[contact_email],
        )

        email.send(fail_silently=False)
        messages.success(self.request, site.contact_success_message)

        return super().form_valid(form)


def workshops(request):
    i_list = Workshop.objects.filter(publish=True)
    site = Site.get_solo()
    context = {
        "i_list": i_list,
        "title": site.title,
        "description": site.social_description,
        "preview": site.preview_image.url,
    }

    return render(request, "cms/workshops.html", context)


class WorkshopDetail(DetailView):
    model = Workshop

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        site = Site.get_solo()
        custom_context = {
            "title": f"{site.title}",
            "description": site.social_description,
            "preview": site.preview_image.url,
            "next": self.object.get_next(),
            "prev": self.object.get_previous(),
        }
        return {**context, **custom_context}


class FrequentQuestionList(ListView):
    model = FrequentQuestion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        site = Site.get_solo()
        custom_context = {
            "title": site.title,
            "description": site.social_description,
            "preview": site.preview_image.url,
        }
        return {**context, **custom_context}

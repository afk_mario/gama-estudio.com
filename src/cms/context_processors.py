from django.urls import reverse


def menu(request):
    menu = {
        "menu": [
            {"name": "tienda", "url": "%s" % reverse("piece-category-list")},
            {"name": "nosotras", "url": "%s" % reverse("us")},
            {"name": "proyectos", "url": "%s" % reverse("projects")},
            {"name": "talleres", "url": "%s" % reverse("workshops")},
        ]
    }
    for item in menu["menu"]:
        if request.path == item["url"]:
            item["active"] = True
    return menu

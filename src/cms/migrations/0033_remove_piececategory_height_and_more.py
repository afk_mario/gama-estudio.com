# Generated by Django 4.1.7 on 2023-03-05 22:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("cms", "0032_piececategory_height_piececategory_image_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="piececategory",
            name="height",
        ),
        migrations.RemoveField(
            model_name="piececategory",
            name="image",
        ),
        migrations.RemoveField(
            model_name="piececategory",
            name="width",
        ),
    ]

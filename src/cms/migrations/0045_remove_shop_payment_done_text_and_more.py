# Generated by Django 4.1.7 on 2023-04-03 02:20

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("cms", "0044_rename_response_text_frequentquestion_answer_text"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="shop",
            name="payment_done_text",
        ),
        migrations.AddField(
            model_name="shop",
            name="payment_done_close_text",
            field=ckeditor.fields.RichTextField(
                blank=True, null=True, verbose_name="Segundo Texto de orden pagada"
            ),
        ),
        migrations.AddField(
            model_name="shop",
            name="payment_done_open_text",
            field=ckeditor.fields.RichTextField(
                blank=True, null=True, verbose_name="Primer Texto de orden pagada"
            ),
        ),
    ]

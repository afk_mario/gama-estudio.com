# Generated by Django 2.2.5 on 2019-09-16 16:17

import cms.models
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0002_auto_20190916_1554'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Projects',
            new_name='Project',
        ),
        migrations.AlterModelOptions(
            name='project',
            options={'ordering': ['order'], 'verbose_name': 'Proyecto', 'verbose_name_plural': 'Proyectos'},
        ),
        migrations.RemoveField(
            model_name='piececategory',
            name='publish',
        ),
        migrations.AddField(
            model_name='us',
            name='image',
            field=models.ImageField(default=datetime.datetime(2019, 9, 16, 16, 17, 36, 832607), upload_to=cms.models.upload_to, verbose_name='Imagen'),
            preserve_default=False,
        ),
    ]

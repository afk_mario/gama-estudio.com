# Generated by Django 4.1.7 on 2023-03-15 01:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("cms", "0041_remove_piece_color_piece_height_piece_length_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="piece",
            name="diameter",
            field=models.FloatField(default=0, verbose_name="diámetro (cm)"),
        ),
        migrations.AlterField(
            model_name="piece",
            name="height",
            field=models.FloatField(default=0, verbose_name="alto (cm)"),
        ),
        migrations.AlterField(
            model_name="piece",
            name="length",
            field=models.FloatField(default=0, verbose_name="largo (cm)"),
        ),
        migrations.AlterField(
            model_name="piece",
            name="weight",
            field=models.FloatField(default=0, verbose_name="peso"),
        ),
        migrations.AlterField(
            model_name="piece",
            name="width",
            field=models.FloatField(default=0, verbose_name="ancho (cm)"),
        ),
    ]

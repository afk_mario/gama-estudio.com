import django_filters

from .models import Piece, PieceCategory


class PieceFilter(django_filters.FilterSet):
    # price__lt = django_filters.NumberFilter(
    #     label="Precio menor a: ", field_name="price", lookup_expr="lt"
    # )
    # price__gt = django_filters.NumberFilter(
    #     label="Precio mayor a:", field_name="price", lookup_expr="gt"
    # )
    # status = django_filters.ChoiceFilter(label="Stock", choices=PIECE_STATUS_OPTIONS)
    category = django_filters.ModelChoiceFilter(
        required=True,
        empty_label=None,
        null_label=None,
        field_name="category",
        to_field_name="slug",
        label="Tipo de pieza:",
        queryset=PieceCategory.objects.all(),
        widget=django_filters.widgets.LinkWidget,
    )

    class Meta:
        model = Piece
        fields = {
            # "status": ["exact"],
            "category": ["exact"],
            # "price": ["lt", "gt"],
        }

from decimal import Decimal

from adminsortable.admin import SortableAdmin, SortableTabularInline
from django.contrib import admin
from django.contrib.admin.widgets import AdminFileWidget
from django.forms import ModelForm
from django.utils.safestring import mark_safe
from django_summernote.admin import SummernoteModelAdmin
from solo.admin import SingletonModelAdmin
from sorl.thumbnail import get_thumbnail

from import_export import resources
from import_export.admin import ImportExportActionModelAdmin

from .models import (
    ExternalShop,
    ExternalShopPiece,
    FrequentQuestion,
    HomeImage,
    Piece,
    PieceCategory,
    PieceImage,
    Project,
    Shop,
    Site,
    Us,
    Workshop,
    WorkshopImage,
)


class ViewOnSiteMixin:
    @mark_safe
    def view_on_site_button(self, obj):
        url = obj.get_absolute_url()
        return f"<a class='button' href='{url}' style='white-space: nowrap;' >view on site</a>"

    view_on_site_button.allow_tags = True
    view_on_site_button.short_description = "View on site"


class AdminImageMixin:
    @mark_safe
    def admin_image(self, obj):
        return f"""
            <img
                src='{obj.image.url}'
                style='height: 100px; width: auto; display: block'
            />
    """

    admin_image.allow_tags = True
    admin_image.short_description = "Preview"


class AdminAdminImageMixin:
    @mark_safe
    def admin_image(self, obj):
        return f"""
            <img
                src='{obj.admin_image()}'
                style='height: 200px; width: auto; display: block'
            />
    """

    admin_image.allow_tags = True
    admin_image.short_description = "Preview"


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        output = []
        if value and getattr(value, "url", None):
            t = get_thumbnail(value, "500x500")
            output.append(f'<img src="{t.url}">')
        output.append(super(AdminFileWidget, self).render(name, value, attrs, renderer))
        return mark_safe("".join(output))


class ImageForm(ModelForm):
    class Meta:
        fields = "__all__"
        model = PieceImage
        widgets = {"image": AdminImageWidget}


class WorkshopImageForm(ModelForm):
    class Meta:
        fields = "__all__"
        model = WorkshopImage
        widgets = {"image": AdminImageWidget}


class PieceImageInline(SortableTabularInline):
    model = PieceImage
    extra = 1
    form = ImageForm


class WorkshopImageInline(SortableTabularInline):
    model = WorkshopImage
    extra = 1
    form = WorkshopImageForm


class ExternalShopPieceInline(admin.TabularInline):
    model = ExternalShopPiece
    extra = 1
    max_num = 1


class PieceResource(resources.ModelResource):
    class Meta:
        model = Piece
        fields = (
            "id",
            "name",
            "slug",
            "category__name",
            "status",
            "price",
            "weight",
            "color",
            "dateUpdated",
        )


class WorkshopResource(resources.ModelResource):
    class Meta:
        model = Workshop
        fields = (
            "id",
            "title",
            "slug",
            "description",
            "status",
            "dateUpdated",
        )


@admin.register(Site)
class SiteAdmin(SingletonModelAdmin, SummernoteModelAdmin):
    summernote_fields = ("contact_text",)
    pass


@admin.register(Shop)
class ShopAdmin(SingletonModelAdmin, SummernoteModelAdmin):
    summernote_fields = (
        "checkout_text",
        "review_order_text",
        "payment_done_open_text",
        "payment_done_close_text",
        "payment_cancelled_text",
    )
    pass


@admin.register(Us)
class UsAdmin(SingletonModelAdmin, SummernoteModelAdmin):
    summernote_fields = ("text",)
    pass


@admin.register(PieceCategory)
class PieceCategoryAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("order", "name", "admin_image")
    list_display_links = ("order", "name", "admin_image")


@admin.register(Piece)
class PieceAdmin(
    SortableAdmin,
    ViewOnSiteMixin,
    AdminAdminImageMixin,
    ImportExportActionModelAdmin,
    SummernoteModelAdmin,
):
    summernote_fields = (
        "summary",
        "description",
    )
    list_per_page = 21
    save_on_top = True
    date_hierarchy = "date_created"
    resource_class = PieceResource
    view_on_site = True
    inlines = [ExternalShopPieceInline, PieceImageInline]
    list_display = (
        "order",
        "publish",
        "name",
        "slug",
        "price_admin",
        "weight",
        "diameter",
        "length",
        "width",
        "height",
        "status",
        "category",
        "admin_image",
        "view_on_site_button",
    )
    list_editable = (
        "publish",
        "status",
        "category",
        "weight",
        "diameter",
        "length",
        "width",
        "height",
    )
    list_display_links = ("order", "name", "slug", "price_admin", "admin_image")
    list_filter = ("publish", "status", "category")
    search_fields = ["name", "slug"]
    actions = [
        "make_sold_out",
        "make_in_stock",
        "make_published",
        "make_unpublished",
        "make_discount_remove",
        "make_discount_5",
        "make_discount_10",
        "make_discount_15",
        "make_discount_20",
    ]

    def price_admin(self, obj):
        if obj.discount_perc == 0:
            return f"${obj.price}"
        else:
            return f"${obj.price} - {obj.discount_perc}% = ${obj.price_discounted}"

    price_admin.short_description = "Precio"

    @admin.action(description="Marcar como sold out")
    def make_sold_out(self, request, queryset):
        queryset.update(status="sold")

    @admin.action(description="Marcar como en stock")
    def make_in_stock(self, request, queryset):
        queryset.update(status="stck")

    @admin.action(description="Publicar")
    def make_published(self, request, queryset):
        queryset.update(publish=True)

    @admin.action(description="Ocultar")
    def make_unpublished(self, request, queryset):
        queryset.update(publish=False)

    @admin.action(description="Remover descuento")
    def make_discount_remove(self, request, queryset):
        queryset.update(discount_perc=Decimal(0))

    @admin.action(description="Agregar 5 de descuento")
    def make_discount_5(self, request, queryset):
        queryset.update(discount_perc=Decimal(5))

    @admin.action(description="Agregar 10 de descuento")
    def make_discount_10(self, request, queryset):
        queryset.update(discount_perc=Decimal(10))

    @admin.action(description="Agregar 15 de descuento")
    def make_discount_15(self, request, queryset):
        queryset.update(discount_perc=Decimal(15))

    @admin.action(description="Agregar 20 de descuento")
    def make_discount_20(self, request, queryset):
        queryset.update(discount_perc=Decimal(20))


@admin.register(Workshop)
class WorkshopAdmin(
    SortableAdmin,
    ViewOnSiteMixin,
    AdminAdminImageMixin,
    ImportExportActionModelAdmin,
    SummernoteModelAdmin,
):
    summernote_fields = (
        "summary",
        "description",
    )
    resource_class = WorkshopResource
    view_on_site = True
    inlines = [WorkshopImageInline]
    list_display = (
        "order",
        "title",
        "publish",
        "admin_image",
        "status",
        "price",
        "view_on_site_button",
    )
    list_editable = ("publish", "status", "price")
    list_display_links = ("order", "title", "admin_image")
    list_filter = ("publish", "status", "title")


@admin.register(PieceImage)
class PieceImageAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("order", "publish", "show_on_home", "admin_image")
    list_editable = (
        "publish",
        "show_on_home",
    )
    list_display_links = ("order", "admin_image")
    list_filter = ("publish", "show_on_home", "piece")


@admin.register(WorkshopImage)
class WorkshopImageAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("order", "publish", "admin_image")
    list_editable = ("publish",)
    list_display_links = ("order", "admin_image")
    list_filter = ("publish", "workshop")


@admin.register(Project)
class ProjectAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("order", "name", "publish", "link", "admin_image")
    list_editable = ("publish",)
    list_display_links = ("order", "name", "link", "admin_image")


@admin.register(HomeImage)
class HomeImageAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("name", "order", "publish", "link", "admin_image")
    list_editable = ("publish",)
    list_display_links = ("name", "order", "link", "admin_image")


@admin.register(FrequentQuestion)
class FrequentQuestionAdmin(SortableAdmin, SummernoteModelAdmin):
    summernote_fields = ("answer_text",)
    list_display = (
        "order",
        "publish",
        "question_text",
    )
    list_display_links = (
        "order",
        "question_text",
    )
    list_editable = ("publish",)
    save_as = True


@admin.register(ExternalShop)
class ExternalShopAdmin(admin.ModelAdmin):
    list_display = ("name", "link")
    list_display_links = ("name", "link")

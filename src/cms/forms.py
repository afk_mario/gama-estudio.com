from captcha.fields import CaptchaField
from django import forms


class ContactForm(forms.Form):
    contact_name = forms.CharField(label="nombre *", required=True)
    contact_email = forms.EmailField(label="correo *", required=True)
    contact_message = forms.CharField(
        label="mensaje *", required=True, widget=forms.Textarea
    )
    captcha = CaptchaField()

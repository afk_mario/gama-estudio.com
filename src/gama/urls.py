from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import include, path

import debug_toolbar
from cms import views
from cms.sitemaps import CatalogSitemap

from .sitemaps import StaticViewSitemap

sitemaps = {"static": StaticViewSitemap, "catalogo": CatalogSitemap}

admin.site.site_header = "Gama Estudio"
admin.site.site_title = "Gama Estudio"
admin.site.index_title = "Admin"


urlpatterns = [
    path("__debug__/", include(debug_toolbar.urls)),
    path("admin/", admin.site.urls),
    path("summernote/", include("django_summernote.urls")),
    path("", views.home, name="home"),
    path("catalogo/", views.CategoryList.as_view(), name="piece-category-list"),
    path("catalogo/<slug:category_slug>/", views.PieceList.as_view(), name="catalog"),
    path(
        "catalogo/<slug:category_slug>/<slug:slug>/",
        views.PieceDetails.as_view(),
        name="catalog_detail",
    ),
    path("talleres/", views.workshops, name="workshops"),
    path(
        "talleres/<slug:slug>", views.WorkshopDetail.as_view(), name="workshop_detail"
    ),
    path("nosotras/", views.us, name="us"),
    path("proyectos/", views.projects, name="projects"),
    path("contacto/", views.Contact.as_view(), name="contact"),
    path("faq/", views.FrequentQuestionList.as_view(), name="faq"),
    path("tienda/", include("shop.urls")),
    path(
        "sitemap.xml",
        sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    path("captcha/", include("captcha.urls")),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

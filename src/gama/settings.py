"""
Django settings for gama project.

Generated by 'django-admin startproject' using Django 2.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

import environ

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

root = environ.Path(__file__) - 3
env = environ.Env(DEBUG=(bool, True))
environ.Env.read_env()

SECRET_KEY = env("SECRET_KEY")
DEBUG = env("DEBUG")
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

if DEBUG:
    DATA_UPLOAD_MAX_NUMBER_FIELDS = 100240
    ALLOWED_HOSTS = ["*"]
    INTERNAL_IPS = "127.0.0.1"
    CSRF_TRUSTED_ORIGINS = ["https://*.ngrok.io"]
else:
    ALLOWED_HOSTS = [".gama-estudio.com", ".ellugar.co", "127.0.0.1", "localhost"]
    CSRF_TRUSTED_ORIGINS = ["https://*.gama-estudio.com"]

ADMINS = [("afk", "afk@ellugar.co")]
MANAGERS = [("Gama Estudio", "admin@gama-estudio.com")]
SECURE_CROSS_ORIGIN_OPENER_POLICY = "same-origin-allow-popups"

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.humanize",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.sitemaps",
    "django.contrib.staticfiles",
    "debug_toolbar",
    "import_export",
    "solo",
    "adminsortable",
    "phonenumber_field",
    "django_summernote",
    "colorfield",
    "imagekit",
    "sorl.thumbnail",
    "django_filters",
    "captcha",
    "cms",
    "shop",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

ROOT_URLCONF = "gama.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "cms.context_processors.menu",
                "shop.context_processors.cart_info",
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "gama.wsgi.application"

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": env("DB_NAME"),
        "USER": env("DB_USER"),
        "PASSWORD": env("DB_PASSWORD"),
        "HOST": env("DB_HOST"),
        "PORT": env("DB_PORT"),
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]


LANGUAGE_CODE = "es-mx"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

USE_S3 = not DEBUG
CORS_ORIGIN_ALLOW_ALL = True
DO_SPACES_ENDPOINT = "nyc3.digitaloceanspaces.com"
AWS_ACCESS_KEY_ID = env("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = env("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = env("AWS_STORAGE_BUCKET_NAME")
AWS_S3_ENDPOINT_URL = f"https://{DO_SPACES_ENDPOINT}"
AWS_S3_CUSTOM_DOMAIN = f"{AWS_STORAGE_BUCKET_NAME}.{DO_SPACES_ENDPOINT}"
AWS_QUERYSTRING_AUTH = False
AWS_DEFAULT_ACL = "public-read"

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "gama/static"),
]

STATICFILES_LOCATION = "static"
MEDIAFILES_LOCATION = "media"

if USE_S3:
    STATIC_URL = f"https://{AWS_S3_CUSTOM_DOMAIN}/{STATICFILES_LOCATION}/"
else:
    STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
    STATIC_URL = "/static/"

MEDIA_ROOT = "media"
MEDIAFILES_LOCATION = "media"
MEDIA_URL = f"https://{AWS_S3_CUSTOM_DOMAIN}/{MEDIAFILES_LOCATION}/"

ADMIN_MEDIA_PREFIX = f"{STATIC_URL}admin/"
THUMBNAIL_PRESERVE_FORMAT = True

STORAGES = {
    "default": {
        "BACKEND": "gama.custom_storages.MediaStorage",
    },
}

if USE_S3:
    STORAGES["staticfiles"] = {
        "BACKEND": "gama.custom_storages.StaticStorage",
    }
else:
    STORAGES["staticfiles"] = {
        "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage"
    }


# Email
if DEBUG:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
else:
    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

SERVER_EMAIL = "tienda@gama-estudio.com"
DEFAULT_FROM_EMAIL = "tienda@gama-estudio.com"
EMAIL_SUBJECT_PREFIX = "[Tienda en linea]"
EMAIL_USE_TLS = True
EMAIL_HOST = env("EMAIL_HOST")
EMAIL_HOST_USER = env("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = env("EMAIL_HOST_PASSWORD")
EMAIL_PORT = env("EMAIL_PORT")

PAYPAL_API_URL = env("PAYPAL_API_URL")
PAYPAL_CLIENT_ID = env("PAYPAL_CLIENT_ID")
PAYPAL_CLIENT_SECRET = env("PAYPAL_CLIENT_SECRET")

SKYDROPX_API_KEY = env("SKYDROPX_API_KEY")
SKYDROPX_API_URL = env("SKYDROPX_API_URL")
SKYDROPX_ZIP_FROM = env("SKYDROPX_ZIP_FROM")

CAPTCHA_IMAGE_SIZE = (200, 40)

PHONENUMBER_DB_FORMAT = "NATIONAL"
PHONENUMBER_DEFAULT_REGION = "MX"

DEBUG_TOOLBAR_CONFIG = {"ROOT_TAG_EXTRA_ATTRS": "hx-preserve"}

SUMMERNOTE_CONFIG = {
    # You can put custom Summernote settings
    "summernote": {
        # Use proper language setting automatically (default)
        "lang": "es-mx",
        # Toolbar customization
        # https://summernote.org/deep-dive/#custom-toolbar-popover
        "toolbar": [
            ["style", ["style"]],
            ["font", ["bold", "italic", "underline", "clear"]],
            ["para", ["ul", "ol", "paragraph"]],
            ["insert", ["link", "picture"]],
            ["view", ["fullscreen", "codeview", "help"]],
        ],
        # Or, explicitly set language/locale for editor
    },
    # Require users to be authenticated for uploading attachments.
    "attachment_require_authentication": True,
}

(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // node_modules/document-ready/index.js
  var require_document_ready = __commonJS({
    "node_modules/document-ready/index.js"(exports, module) {
      "use strict";
      module.exports = ready;
      function ready(callback) {
        if (typeof document === "undefined") {
          throw new Error("document-ready only runs in the browser");
        }
        var state = document.readyState;
        if (state === "complete" || state === "interactive") {
          return setTimeout(callback, 0);
        }
        document.addEventListener("DOMContentLoaded", function onLoad() {
          callback();
        });
      }
    }
  });

  // static-src/js/process-payment.js
  var import_document_ready = __toESM(require_document_ready());

  // node_modules/@paypal/paypal-js/dist/esm/paypal-js.js
  function findScript(url, attributes) {
    var currentScript = document.querySelector('script[src="'.concat(url, '"]'));
    if (currentScript === null)
      return null;
    var nextScript = createScriptElement(url, attributes);
    var currentScriptClone = currentScript.cloneNode();
    delete currentScriptClone.dataset.uidAuto;
    if (Object.keys(currentScriptClone.dataset).length !== Object.keys(nextScript.dataset).length) {
      return null;
    }
    var isExactMatch = true;
    Object.keys(currentScriptClone.dataset).forEach(function(key) {
      if (currentScriptClone.dataset[key] !== nextScript.dataset[key]) {
        isExactMatch = false;
      }
    });
    return isExactMatch ? currentScript : null;
  }
  function insertScriptElement(_a) {
    var url = _a.url, attributes = _a.attributes, onSuccess = _a.onSuccess, onError = _a.onError;
    var newScript = createScriptElement(url, attributes);
    newScript.onerror = onError;
    newScript.onload = onSuccess;
    document.head.insertBefore(newScript, document.head.firstElementChild);
  }
  function processOptions(options) {
    var sdkBaseURL = "https://www.paypal.com/sdk/js";
    if (options.sdkBaseURL) {
      sdkBaseURL = options.sdkBaseURL;
      delete options.sdkBaseURL;
    }
    processMerchantID(options);
    var _a = Object.keys(options).filter(function(key) {
      return typeof options[key] !== "undefined" && options[key] !== null && options[key] !== "";
    }).reduce(function(accumulator, key) {
      var value = options[key].toString();
      if (key.substring(0, 5) === "data-") {
        accumulator.dataAttributes[key] = value;
      } else {
        accumulator.queryParams[key] = value;
      }
      return accumulator;
    }, {
      queryParams: {},
      dataAttributes: {}
    }), queryParams = _a.queryParams, dataAttributes = _a.dataAttributes;
    return {
      url: "".concat(sdkBaseURL, "?").concat(objectToQueryString(queryParams)),
      dataAttributes
    };
  }
  function objectToQueryString(params) {
    var queryString = "";
    Object.keys(params).forEach(function(key) {
      if (queryString.length !== 0)
        queryString += "&";
      queryString += key + "=" + params[key];
    });
    return queryString;
  }
  function parseErrorMessage(message) {
    var originalErrorText = message.split("/* Original Error:")[1];
    return originalErrorText ? originalErrorText.replace(/\n/g, "").replace("*/", "").trim() : message;
  }
  function createScriptElement(url, attributes) {
    if (attributes === void 0) {
      attributes = {};
    }
    var newScript = document.createElement("script");
    newScript.src = url;
    Object.keys(attributes).forEach(function(key) {
      newScript.setAttribute(key, attributes[key]);
      if (key === "data-csp-nonce") {
        newScript.setAttribute("nonce", attributes["data-csp-nonce"]);
      }
    });
    return newScript;
  }
  function processMerchantID(options) {
    var merchantID = options["merchant-id"], dataMerchantID = options["data-merchant-id"];
    var newMerchantID = "";
    var newDataMerchantID = "";
    if (Array.isArray(merchantID)) {
      if (merchantID.length > 1) {
        newMerchantID = "*";
        newDataMerchantID = merchantID.toString();
      } else {
        newMerchantID = merchantID.toString();
      }
    } else if (typeof merchantID === "string" && merchantID.length > 0) {
      newMerchantID = merchantID;
    } else if (typeof dataMerchantID === "string" && dataMerchantID.length > 0) {
      newMerchantID = "*";
      newDataMerchantID = dataMerchantID;
    }
    options["merchant-id"] = newMerchantID;
    options["data-merchant-id"] = newDataMerchantID;
    return options;
  }
  function loadScript(options, PromisePonyfill) {
    if (PromisePonyfill === void 0) {
      PromisePonyfill = getDefaultPromiseImplementation();
    }
    validateArguments(options, PromisePonyfill);
    if (typeof window === "undefined")
      return PromisePonyfill.resolve(null);
    var _a = processOptions(options), url = _a.url, dataAttributes = _a.dataAttributes;
    var namespace = dataAttributes["data-namespace"] || "paypal";
    var existingWindowNamespace = getPayPalWindowNamespace(namespace);
    if (findScript(url, dataAttributes) && existingWindowNamespace) {
      return PromisePonyfill.resolve(existingWindowNamespace);
    }
    return loadCustomScript({
      url,
      attributes: dataAttributes
    }, PromisePonyfill).then(function() {
      var newWindowNamespace = getPayPalWindowNamespace(namespace);
      if (newWindowNamespace) {
        return newWindowNamespace;
      }
      throw new Error("The window.".concat(namespace, " global variable is not available."));
    });
  }
  function loadCustomScript(options, PromisePonyfill) {
    if (PromisePonyfill === void 0) {
      PromisePonyfill = getDefaultPromiseImplementation();
    }
    validateArguments(options, PromisePonyfill);
    var url = options.url, attributes = options.attributes;
    if (typeof url !== "string" || url.length === 0) {
      throw new Error("Invalid url.");
    }
    if (typeof attributes !== "undefined" && typeof attributes !== "object") {
      throw new Error("Expected attributes to be an object.");
    }
    return new PromisePonyfill(function(resolve, reject) {
      if (typeof window === "undefined")
        return resolve();
      insertScriptElement({
        url,
        attributes,
        onSuccess: function() {
          return resolve();
        },
        onError: function() {
          var defaultError = new Error('The script "'.concat(url, '" failed to load.'));
          if (!window.fetch) {
            return reject(defaultError);
          }
          return fetch(url).then(function(response) {
            if (response.status === 200) {
              reject(defaultError);
            }
            return response.text();
          }).then(function(message) {
            var parseMessage = parseErrorMessage(message);
            reject(new Error(parseMessage));
          }).catch(function(err) {
            reject(err);
          });
        }
      });
    });
  }
  function getDefaultPromiseImplementation() {
    if (typeof Promise === "undefined") {
      throw new Error("Promise is undefined. To resolve the issue, use a Promise polyfill.");
    }
    return Promise;
  }
  function getPayPalWindowNamespace(namespace) {
    return window[namespace];
  }
  function validateArguments(options, PromisePonyfill) {
    if (typeof options !== "object" || options === null) {
      throw new Error("Expected an options object.");
    }
    if (typeof PromisePonyfill !== "undefined" && typeof PromisePonyfill !== "function") {
      throw new Error("Expected PromisePonyfill to be a function.");
    }
  }

  // node_modules/js-cookie/dist/js.cookie.mjs
  function assign(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        target[key] = source[key];
      }
    }
    return target;
  }
  var defaultConverter = {
    read: function(value) {
      if (value[0] === '"') {
        value = value.slice(1, -1);
      }
      return value.replace(/(%[\dA-F]{2})+/gi, decodeURIComponent);
    },
    write: function(value) {
      return encodeURIComponent(value).replace(
        /%(2[346BF]|3[AC-F]|40|5[BDE]|60|7[BCD])/g,
        decodeURIComponent
      );
    }
  };
  function init(converter, defaultAttributes) {
    function set(key, value, attributes) {
      if (typeof document === "undefined") {
        return;
      }
      attributes = assign({}, defaultAttributes, attributes);
      if (typeof attributes.expires === "number") {
        attributes.expires = new Date(Date.now() + attributes.expires * 864e5);
      }
      if (attributes.expires) {
        attributes.expires = attributes.expires.toUTCString();
      }
      key = encodeURIComponent(key).replace(/%(2[346B]|5E|60|7C)/g, decodeURIComponent).replace(/[()]/g, escape);
      var stringifiedAttributes = "";
      for (var attributeName in attributes) {
        if (!attributes[attributeName]) {
          continue;
        }
        stringifiedAttributes += "; " + attributeName;
        if (attributes[attributeName] === true) {
          continue;
        }
        stringifiedAttributes += "=" + attributes[attributeName].split(";")[0];
      }
      return document.cookie = key + "=" + converter.write(value, key) + stringifiedAttributes;
    }
    function get(key) {
      if (typeof document === "undefined" || arguments.length && !key) {
        return;
      }
      var cookies = document.cookie ? document.cookie.split("; ") : [];
      var jar = {};
      for (var i = 0; i < cookies.length; i++) {
        var parts = cookies[i].split("=");
        var value = parts.slice(1).join("=");
        try {
          var foundKey = decodeURIComponent(parts[0]);
          jar[foundKey] = converter.read(value, foundKey);
          if (key === foundKey) {
            break;
          }
        } catch (e) {
        }
      }
      return key ? jar[key] : jar;
    }
    return Object.create(
      {
        set,
        get,
        remove: function(key, attributes) {
          set(
            key,
            "",
            assign({}, attributes, {
              expires: -1
            })
          );
        },
        withAttributes: function(attributes) {
          return init(this.converter, assign({}, this.attributes, attributes));
        },
        withConverter: function(converter2) {
          return init(assign({}, this.converter, converter2), this.attributes);
        }
      },
      {
        attributes: { value: Object.freeze(defaultAttributes) },
        converter: { value: Object.freeze(converter) }
      }
    );
  }
  var api = init(defaultConverter, { path: "/" });
  var js_cookie_default = api;

  // static-src/js/process-payment.js
  (0, import_document_ready.default)(async () => {
    try {
      const csrftoken = js_cookie_default.get("csrftoken");
      const paypal = await loadScript({
        // eslint-disable-next-line no-undef
        "client-id": PAYPAL_CLIENT_ID,
        currency: "MXN",
        locale: "es_MX"
      });
      paypal.Buttons({
        style: {
          size: "small",
          shape: "rect",
          color: "gold",
          layout: "vertical",
          label: "paypal",
          height: 40
        },
        async createOrder() {
          const res = await fetch(PAYPAL_CREATE_ORDER_URL, {
            method: "post",
            credentials: "same-origin",
            headers: { "X-CSRFToken": csrftoken }
          });
          const order = await res.json();
          return order.id;
        },
        async onApprove(data) {
          try {
            const res = await fetch(PAYPAL_VERIFY_ORDER_URL, {
              method: "POST",
              credentials: "same-origin",
              headers: { "X-CSRFToken": csrftoken },
              body: JSON.stringify({
                orderID: data.orderID
              })
            });
            if (res.status !== 200) {
              throw new Error(res.statusText);
            }
          } catch (e) {
            console.error("Error verifiying the order");
          }
          try {
            const res = await fetch(PAYPAL_CAPTURE_ORDER_URL, {
              method: "POST",
              credentials: "same-origin",
              headers: { "X-CSRFToken": csrftoken },
              body: JSON.stringify({
                orderID: data.orderID
              })
            });
            if (res.status === 200) {
              window.location.href = PAYMENT_DONE_URL;
            } else {
              throw new Error(res.statusText);
            }
          } catch (error) {
            console.error("Error capturing the order");
          }
        },
        onError: function(err) {
          console.error(err);
          window.location.href = PAYMENT_CANCELLED_URL;
        }
      }).render("#paypal-button-container");
    } catch (err) {
      console.error("failed to load the PayPal JS SDK script", err);
    }
  });
})();
/*! Bundled license information:

@paypal/paypal-js/dist/esm/paypal-js.js:
  (*!
   * paypal-js v5.1.4 (2022-11-29T23:08:21.847Z)
   * Copyright 2020-present, PayPal, Inc. All rights reserved.
   *
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *
   * http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   *)

js-cookie/dist/js.cookie.mjs:
  (*! js-cookie v3.0.1 | MIT *)
*/

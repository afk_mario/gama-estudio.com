import Ready from "document-ready";
import Swiper, { Autoplay, EffectFade } from "swiper";

Ready(() => {
  // eslint-disable-next-line no-unused-vars
  const swiper = new Swiper(".swiper", {
    modules: [Autoplay, EffectFade],
    loop: true,
    speed: 2000,
    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
  });
});

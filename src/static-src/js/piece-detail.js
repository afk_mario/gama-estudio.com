import Ready from "document-ready";
import GLightbox from "glightbox";

Ready(() => {
  GLightbox({
    touchNavigation: true,
    loopAtEnd: true,
    title: false,
  });
});

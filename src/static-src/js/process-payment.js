import Ready from "document-ready";
import { loadScript } from "@paypal/paypal-js";
import Cookies from "js-cookie";

Ready(async () => {
  try {
    const csrftoken = Cookies.get("csrftoken");
    const paypal = await loadScript({
      // eslint-disable-next-line no-undef
      "client-id": PAYPAL_CLIENT_ID,
      currency: "MXN",
      locale: "es_MX",
    });

    paypal
      .Buttons({
        style: {
          size: "small",
          shape: "rect",
          color: "gold",
          layout: "vertical",
          label: "paypal",
          height: 40,
        },
        async createOrder() {
          // eslint-disable-next-line no-undef
          const res = await fetch(PAYPAL_CREATE_ORDER_URL, {
            method: "post",
            credentials: "same-origin",
            headers: { "X-CSRFToken": csrftoken },
          });
          const order = await res.json();
          return order.id;
        },

        async onApprove(data) {
          try {
            // eslint-disable-next-line no-undef
            const res = await fetch(PAYPAL_VERIFY_ORDER_URL, {
              method: "POST",
              credentials: "same-origin",
              headers: { "X-CSRFToken": csrftoken },
              body: JSON.stringify({
                orderID: data.orderID,
              }),
            });
            if (res.status !== 200) {
              throw new Error(res.statusText);
            }
          } catch (e) {
            console.error("Error verifiying the order");
          }
          try {
            // eslint-disable-next-line no-undef
            const res = await fetch(PAYPAL_CAPTURE_ORDER_URL, {
              method: "POST",
              credentials: "same-origin",
              headers: { "X-CSRFToken": csrftoken },
              body: JSON.stringify({
                orderID: data.orderID,
              }),
            });
            if (res.status === 200) {
              // eslint-disable-next-line no-undef
              window.location.href = PAYMENT_DONE_URL;
            } else {
              throw new Error(res.statusText);
            }
          } catch (error) {
            console.error("Error capturing the order");
          }
        },

        onError: function (err) {
          console.error(err);
          // eslint-disable-next-line no-undef
          window.location.href = PAYMENT_CANCELLED_URL;
        },
      })
      .render("#paypal-button-container");
  } catch (err) {
    console.error("failed to load the PayPal JS SDK script", err);
  }
});

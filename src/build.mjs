import * as esbuild from "esbuild";

const ctx = await esbuild.context({
  entryPoints: [
    "static-src/js/home.js",
    "static-src/js/process-payment.js",
    "static-src/js/piece-detail.js",
    "static-src/css/styles.css",
  ],
  bundle: true,
  outdir: "gama/static/",
  logLevel: "info",
});

await ctx.watch();
console.log("watching...");

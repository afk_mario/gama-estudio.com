from django import forms
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import RegionalPhoneNumberWidget

from .constants import MEXICO_STATES_OPTIONS


class UpdateOrderShippingCostForm(forms.Form):
    order_id = forms.IntegerField()


class ProductActionForm(forms.Form):
    product_id = forms.IntegerField(label="Product Id")


class CheckoutForm(forms.Form):
    name = forms.CharField(max_length=240)
    email = forms.EmailField()
    rfc = forms.CharField(label="RFC", max_length=15)
    phone_number = PhoneNumberField(
        region="MX",
        widget=RegionalPhoneNumberWidget(
            attrs={"placeholder": "## #### ####"},
        ),
    )
    shipping_address_id = forms.IntegerField()
    postal_code = forms.IntegerField(label="código postal")
    address_01 = forms.CharField(label="dirección", max_length=240)
    address_02 = forms.CharField(
        label="casa, apartamento, etc", max_length=240, required=False
    )
    state = forms.ChoiceField(label="estado", choices=MEXICO_STATES_OPTIONS)
    city = forms.CharField(label="ciudad", max_length=240)
    references = forms.CharField(
        label="referencias", widget=forms.Textarea, required=False
    )

from django.db import models


class OrderStatus(models.TextChoices):
    OPEN = "OPN", "Open"
    ARCHIVED = "ARC", "Archived"
    CANCELED = "CAN", "Canceled"


class PaymentStatus(models.TextChoices):
    UNPAID = "UNP", "Unpaid"
    PENDING = "PEN", "Pending"
    PAID = "PAI", "Paid"
    REFUNDED = "REF", "Refunded"
    VOIDED = "VOI", "Voided"


# https://developer.paypal.com/api/rest/reference/state-codes/#link-mexico
MEXICO_STATES_OPTIONS = (
    ("AGS", "Aguascalientes"),
    ("BC", "Baja California"),
    ("BCS", "Baja California Sur"),
    ("CAMP", "Campeche"),
    ("CHIS", "Chiapas"),
    ("CHIH", "Chihuahua"),
    ("CDMX", "Ciudad de México"),
    ("COAH", "Coahuila"),
    ("COL", "Colima"),
    ("DGO", "Durango"),
    ("MEX", "Estado de México"),
    ("GTO", "Guanajuato"),
    ("GRO", "Guerrero"),
    ("HGO", "Hidalgo"),
    ("JAL", "Jalisco"),
    ("MICH", "Michoacán"),
    ("MOR", "Morelos"),
    ("NAY", "Nayarit"),
    ("NL", "Nuevo León"),
    ("OAX", "Oaxaca"),
    ("PUE", "Puebla"),
    ("QRO", "Querétaro"),
    ("Q ROO", "Quintana Roo"),
    ("SLP", "San Luis Potosí"),
    ("SIN", "Sinaloa"),
    ("SON", "Sonora"),
    ("TAB", "Tabasco"),
    ("TAMPS", "Tamaulipas"),
    ("TLAX", "Tlaxcala"),
    ("VER", "Veracruz"),
    ("YUC", "Yucatán"),
    ("ZAC", "Zacatecas"),
)

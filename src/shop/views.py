import json
import uuid

import requests
from django.conf import settings
from django.core.exceptions import BadRequest
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.views.generic import DetailView, FormView, View

from cms.models import Piece

from .constants import PaymentStatus
from .forms import CheckoutForm, ProductActionForm, UpdateOrderShippingCostForm
from .helpers import (
    customer_get_or_404,
    customer_get_or_create,
    order_handle_success,
)
from .models import Order, OrderItem, ShippingAddress
from .paypal import get_pay_pal_dict


class Cart(View):
    def get(self, request, *args, **kwargs):
        return render(request, "shop/cart.html", {})

    # Create new order item
    def post(self, request, *args, **kwargs):
        request = self.request
        form = ProductActionForm(request.POST)
        if form.is_valid():
            product_id = form.cleaned_data["product_id"]

            customer = customer_get_or_create(request)

            product = get_object_or_404(Piece, id=product_id)
            order, created = Order.objects.get_or_create(
                customer=customer, payment_status=PaymentStatus.UNPAID
            )
            # TODO: if the order item is not created it was already on the cart
            order_item, created = OrderItem.objects.get_or_create(
                order=order, product=product
            )

            response = render(request, "shop/partials/cart-add-success.html")
            response["HX-Trigger-After-Swap"] = "cart-updated"
            return response
        else:
            raise BadRequest("Invalid request.")


@require_http_methods(["GET"])
def cart_widget(request):
    return render(request, "shop/partials/cart-widget.html")


@require_http_methods(["GET"])
def cart_details(request):
    return render(request, "shop/partials/cart-details.html", {})


class CartItem(View):
    def get(self, request, *args, **kwargs):
        item_id = kwargs["item_id"]
        order_item = get_object_or_404(OrderItem, id=item_id)
        context = {"item": order_item}
        return render(request, "shop/partials/cart-row.html", context)

    def delete(self, request, *args, **kwargs):
        item_id = kwargs["item_id"]
        order_item = get_object_or_404(OrderItem, id=item_id)
        order_item.delete()

        response = render(request, "shop/partials/cart-details.html", {})
        response["HX-Trigger-After-Swap"] = "cart-updated"
        return response


class Checkout(FormView):
    template_name = "shop/checkout.html"
    form_class = CheckoutForm

    def get_success_url(self):
        return reverse("process_payment")

    def get_initial(self):
        customer = customer_get_or_404(self.request)
        order = get_object_or_404(
            Order, customer=customer, payment_status=PaymentStatus.UNPAID
        )

        if not order.shipping_address:
            prev_shipping_address = ShippingAddress.objects.filter(
                customer=customer
            ).last()
            defaults = {}

            if prev_shipping_address:
                defaults = prev_shipping_address.__dict__
                defaults.pop("id")
                defaults.pop("_state")

            new_shipping_address = ShippingAddress(**defaults)
            new_shipping_address.save()
            order.shipping_address = new_shipping_address
            order.save()

        return {
            "name": customer.name,
            "email": customer.email,
            "rfc": customer.rfc,
            "phone_number": customer.phone_number,
            "postal_code": order.shipping_address.postal_code,
            "address_01": order.shipping_address.address_01,
            "address_02": order.shipping_address.address_02,
            "state": order.shipping_address.state,
            "city": order.shipping_address.city,
            "references": order.shipping_address.references,
            "shipping_address_id": order.shipping_address.id,
        }

    def form_valid(self, form):
        customer = customer_get_or_404(self.request)
        order = get_object_or_404(
            Order, customer=customer, payment_status=PaymentStatus.UNPAID
        )

        cleaned_data = form.cleaned_data

        customer.name = cleaned_data.get("name")
        customer.email = cleaned_data.get("email")
        customer.phone_number = cleaned_data.get("phone_number")
        customer.rfc = cleaned_data.get("rfc")
        customer.save()

        shipping_address = get_object_or_404(
            ShippingAddress, id=cleaned_data.get("shipping_address_id")
        )
        shipping_address.customer = customer
        shipping_address.postal_code = cleaned_data.get("postal_code")
        shipping_address.address_01 = cleaned_data.get("address_01")
        shipping_address.address_02 = cleaned_data.get("address_02")
        shipping_address.state = cleaned_data.get("state")
        shipping_address.city = cleaned_data.get("city")
        shipping_address.references = cleaned_data.get("references")
        shipping_address.save()

        order.shipping_address = shipping_address
        order.save()

        return super().form_valid(form)


def process_payment(request):
    return render(
        request,
        "shop/process-payment.html",
        {"PAYPAL_CLIENT_ID": settings.PAYPAL_CLIENT_ID},
    )


def checkout_order_details(request):
    context = {}
    return render(request, "shop/partials/checkout-order-details.html", context)


@require_http_methods(["POST"])
def paypal_create_order(request):
    customer = customer_get_or_404(request)
    order = get_object_or_404(
        Order, customer=customer, payment_status=PaymentStatus.UNPAID
    )
    data = json.dumps(get_pay_pal_dict(request))

    headers = {
        "Content-Type": "application/json",
        "PayPal-Request-Id": str(uuid.uuid4()),
    }

    try:
        res = requests.post(
            f"{settings.PAYPAL_API_URL}/v2/checkout/orders",
            headers=headers,
            auth=(settings.PAYPAL_CLIENT_ID, settings.PAYPAL_CLIENT_SECRET),
            data=data,
        )

        res.raise_for_status()
        res_data = res.json()
        transaction_id = res_data.get("id")
        order.transaction_id = transaction_id
        order.save()

        response = JsonResponse(res_data)
        return response

    except requests.exceptions.HTTPError as err:
        return HttpResponse(status=err.response.status_code)


@require_http_methods(["POST"])
def paypal_capture_order(request):
    request_data = json.loads(request.body)
    transaction_id = request_data.get("orderID")
    customer = customer_get_or_404(request)
    order = get_object_or_404(
        Order, customer=customer, payment_status=PaymentStatus.UNPAID
    )
    if order.transaction_id != transaction_id:
        return HttpResponse(status=400)
    try:
        data = "{}"
        headers = {
            "Content-Type": "application/json",
            "PayPal-Request-Id": str(uuid.uuid4()),
        }
        res = requests.post(
            f"{settings.PAYPAL_API_URL}/v2/checkout/orders/{transaction_id}/capture",
            headers=headers,
            auth=(settings.PAYPAL_CLIENT_ID, settings.PAYPAL_CLIENT_SECRET),
            data=data,
        )

        res.raise_for_status()
        response_data = res.json()
        status = response_data.get("status")

        if status == "COMPLETED":
            order_handle_success(order)

        return HttpResponse(status=200)

    except requests.exceptions.HTTPError as err:
        return HttpResponse(status=err.response.status_code)


@require_http_methods(["POST"])
def paypal_verify_order(request):
    request_data = json.loads(request.body)
    transaction_id = request_data.get("orderID")
    customer = customer_get_or_404(request)
    order = get_object_or_404(
        Order, customer=customer, payment_status=PaymentStatus.UNPAID
    )

    if order.transaction_id != transaction_id:
        return HttpResponse(status=400)
    try:
        res = requests.get(
            f"{settings.PAYPAL_API_URL}/v2/checkout/orders/{transaction_id}",
            auth=(settings.PAYPAL_CLIENT_ID, settings.PAYPAL_CLIENT_SECRET),
        )

        res.raise_for_status()
        response_data = res.json()
        status = response_data.get("status")

        if status == "APPROVED":
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=502)

    except requests.exceptions.HTTPError as err:
        return HttpResponse(status=err.response.status_code)


class OrderDetails(DetailView):
    model = Order

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # site = Site.get_solo()
        custom_context = {
            # "title": f"{site.title} | {self.object.name}",
            # "description": site.social_description,
            # "preview": site.preview_image.url,
        }
        return {**context, **custom_context}


def payment_done(request):
    return render(request, "shop/payment-done.html")


def payment_canceled(request):
    return render(request, "shop/payment-cancelled.html")


@require_http_methods(["POST"])
def update_order_shipping_cost(request, pk):
    print("hey")
    form = UpdateOrderShippingCostForm(request.POST)

    if not form.is_valid():
        print("not valid form")
        return render(
            request,
            "shop/partials/checkout-order-shipping-cost-error.html",
            {"form": form},
        )

    order = get_object_or_404(Order, id=form.cleaned_data.get("order_id"))

    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Token {settings.SKYDROPX_API_KEY}",
    }
    data = json.dumps(
        {
            "zip_from": f"{settings.SKYDROPX_ZIP_FROM}",
            "zip_to": order.shipping_address.postal_code,
            "carriers": [{"name": "Fedex"}],
            "parcel": {
                "weight": f"{order.get_weight}",
                "height": "10",
                "width": "10",
                "length": "10",
            },
        }
    )

    res = requests.post(
        f"{settings.SKYDROPX_API_URL}/v1/quotations",
        headers=headers,
        data=data,
    )

    res.raise_for_status()
    res_data = res.json()
    print(res_data)
    shipping_cost = res_data[0].get("amount_local", 0)
    order.shipping_cost = shipping_cost

    print(res_data)
    return render(
        request,
        "shop/partials/checkout-order-shipping-cost.html",
        {"shipping_cost": shipping_cost},
    )

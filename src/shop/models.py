from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from solo.models import SingletonModel

from cms.models import Piece

from .constants import (
    MEXICO_STATES_OPTIONS,
    OrderStatus,
    PaymentStatus,
)
from .shipping import calculate_order_weight


class Customer(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    session_id = models.CharField(max_length=240, null=True)
    name = models.CharField(default="", max_length=240, null=True, blank=True)
    email = models.EmailField(default="", null=True, blank=True)
    phone_number = PhoneNumberField(
        "número de teléfono",
        blank=True,
        default="",
    )
    rfc = models.CharField("RFC", default="", max_length=240, null=True, blank=True)

    def __str__(self):
        if self.name:
            return self.name

        return f"Anonymous: {self.session_id}"


class ShippingCostInfo(SingletonModel):
    shipping_cost_extra = models.DecimalField(
        "Precio por kg extra (MXN)", default=450, max_digits=7, decimal_places=2
    )

    @classmethod
    def get_cost_by_weight(cls, weight):
        max_cost = ShippingCost.get_closest_weight(weight)
        if max_cost:
            remain = max(weight - max_cost.weight, 0)
            extra = cls.get_solo().shipping_cost_extra
            return max_cost.price + (extra * remain)
        else:
            return 0


class ShippingCost(models.Model):
    info = models.ForeignKey(ShippingCostInfo, on_delete=models.CASCADE)
    weight = models.PositiveIntegerField("Peso (kg)", unique=True)
    price = models.DecimalField(
        "Precio (MXN)", default=0, max_digits=7, decimal_places=2
    )

    class Meta:
        ordering = ["weight"]

    def __str__(self):
        return f"{self.weight}kg= ${self.price}MXN"

    @classmethod
    def get_closest_weight(cls, target_weight):
        closest_shipping = (
            cls.objects.filter(weight__gte=target_weight).order_by("weight").first()
        )

        if (
            not closest_shipping
        ):  # If no weight >= target_weight, get the max weight available
            closest_shipping = cls.objects.order_by("-weight").first()

        return closest_shipping


class ShippingAddress(models.Model):
    customer = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, blank=True, null=True
    )
    postal_code = models.CharField(
        "código postal", max_length=240, blank=True, default=""
    )
    address_01 = models.CharField("dirección", max_length=240, blank=True, default="")
    address_02 = models.CharField(
        "casa, apartamento, etc", max_length=240, blank=True, default=""
    )
    state = models.CharField(
        "estado",
        max_length=20,
        blank="True",
        default="",
        choices=MEXICO_STATES_OPTIONS,
    )
    city = models.CharField("ciudad", max_length=240, blank=True, default="")
    references = models.TextField("referecias", blank=True, default="")

    def __str__(self):
        return f"{self.address_01} {self.state} {self.city}"


class Order(models.Model):
    customer = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, blank=True, null=True
    )
    date_created = models.DateTimeField(auto_now_add=True)
    payment_status = models.CharField(
        choices=PaymentStatus.choices, max_length=50, default=PaymentStatus.UNPAID
    )
    order_status = models.CharField(
        choices=OrderStatus.choices, max_length=50, default=OrderStatus.OPEN
    )
    shipping_address = models.ForeignKey(
        ShippingAddress, on_delete=models.SET_NULL, blank=True, null=True
    )
    transaction_id = models.CharField(max_length=200, null=True)
    subtotal_cost = models.DecimalField(
        default=None,
        max_digits=7,
        decimal_places=2,
        editable=False,
        null=True,
    )
    shipping_cost = models.DecimalField(
        default=None,
        max_digits=7,
        decimal_places=2,
        editable=False,
        null=True,
    )

    def __str__(self):
        return f"Order {self.id}"

    def get_absolute_url(self):
        return reverse("order_details", kwargs={"pk": self.pk})

    @classmethod
    def from_db(cls, db, field_names, values):
        instance = super().from_db(db, field_names, values)

        # save original values, when model is loaded from database,
        # in a separate attribute on the model
        instance._loaded_values = dict(zip(field_names, values, strict=False))

        return instance

    def save(self, *args, **kwargs):
        if (
            not self._state.adding
            and (self.payment_status != self._loaded_values["payment_status"])
            and self.payment_status == PaymentStatus.PAID
        ):
            self.subtotal_cost = self.get_subtotal_cost_by_stock
            self.shipping_cost = self.get_shipping_cost_by_stock
        super().save(*args, **kwargs)

    @property
    def get_weight(self):
        items = self.orderitem_set.filter(product__status="stck")
        return calculate_order_weight(items)

    @property
    def get_subtotal_cost_by_stock(self):
        items = self.orderitem_set.filter(product__status="stck")
        return sum([item.get_total_cost for item in items])

    @property
    def get_shipping_cost_by_stock(self):
        items = self.orderitem_set.filter(product__status="stck")
        weight = calculate_order_weight(items)
        price = ShippingCostInfo.get_cost_by_weight(weight)
        return price

    @property
    def get_total_cost_by_stock(self):
        subtotal = self.get_subtotal_cost_by_stock
        shipping = self.get_shipping_cost_by_stock
        return subtotal + shipping

    @property
    def get_subtotal_cost(self):
        if self.payment_status == PaymentStatus.PAID:
            return self.subtotal_cost

        return self.get_subtotal_cost_by_stock

    get_subtotal_cost.fget.short_description = "Subtotal"

    @property
    def get_shipping_cost(self):
        if self.payment_status == PaymentStatus.PAID:
            return self.shipping_cost

        return self.get_shipping_cost_by_stock

    get_shipping_cost.fget.short_description = "Shipping"

    @property
    def get_total_cost(self):
        if self.payment_status == PaymentStatus.PAID:
            return self.subtotal_cost + self.shipping_cost

        return self.get_total_cost_by_stock

    get_total_cost.fget.short_description = "Total"

    @property
    def get_cart_items_count(self):
        items = self.orderitem_set.all()
        return len(items)


class OrderItem(models.Model):
    product = models.ForeignKey(Piece, on_delete=models.SET_NULL, blank=True, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, blank=True, null=True)
    quantity = models.IntegerField(default=1)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        if self.product:
            return f"Order Item for {self.product.name}"
        return "Order Item --"

    @property
    def get_total_cost(self):
        return self.product.price_discounted * self.quantity

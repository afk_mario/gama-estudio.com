from decimal import Decimal

from django.shortcuts import get_object_or_404
from django.urls import reverse

from .constants import PaymentStatus
from .helpers import customer_get_or_404
from .models import Order


def get_pay_pal_dict(request):
    host = request.get_host()
    scheme = request.scheme

    customer = customer_get_or_404(request)
    order = get_object_or_404(
        Order, customer=customer, payment_status=PaymentStatus.UNPAID
    )
    items = order.orderitem_set.filter(product__status="stck")
    shipping_address = order.shipping_address

    return {
        "id": order.id,
        "intent": "CAPTURE",
        "application_context": {
            "brand_name": "Gama Estudio",
            "locale": "es-MX",
            "shipping_preference": "SET_PROVIDED_ADDRESS",
        },
        "purchase_units": [
            {
                "invoice_number": order.id,
                "reference_id": order.id,
                "description": f"Orden de compra Gama Estudio NO {order.id}",
                "amount": {
                    "currency_code": "MXN",
                    "value": "%.2f"
                    % Decimal(order.get_total_cost).quantize(Decimal(".01")),
                    "breakdown": {
                        "item_total": {
                            "currency_code": "MXN",
                            "value": "%.2f"
                            % Decimal(order.get_subtotal_cost).quantize(Decimal(".01")),
                        },
                        "shipping": {
                            "currency_code": "MXN",
                            "value": "%.2f"
                            % Decimal(order.get_shipping_cost).quantize(Decimal(".01")),
                        },
                    },
                },
                "payer": {"email_address": customer.email},
                "items": [
                    {
                        "name": item.product.name,
                        "sku": item.id,
                        "quantity": "1",
                        "unit_amount": {
                            "currency_code": "MXN",
                            "value": "%.2f"
                            % Decimal(item.product.price_discounted).quantize(
                                Decimal(".01")
                            ),
                        },
                    }
                    for item in items
                ],
                "shipping": {
                    "type": "SHIPPING",
                    "address": {
                        "address_line_1": shipping_address.address_01,
                        "address_line_2": shipping_address.address_02,
                        "admin_area_1": shipping_address.state,
                        "admin_area_2": shipping_address.city,
                        "country_code": "MX",
                        "postal_code": shipping_address.postal_code,
                    },
                },
            }
        ],
        "redirect_urls": {
            "return_url": f"{scheme}://{host}{reverse('payment_done')}",
            "cancel_url": f"{scheme}://{host}{reverse('payment_cancelled')}",
        },
    }

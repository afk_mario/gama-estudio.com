from django.urls import path

from . import views

urlpatterns = [
    path("carrito", views.Cart.as_view(), name="cart"),
    path("carrito/<int:item_id>", views.CartItem.as_view(), name="cart_item"),
    path("carrito/detalles", views.cart_details, name="cart_details"),
    path("carrito/widget", views.cart_widget, name="cart_widget"),
    path("checkout", views.Checkout.as_view(), name="checkout"),
    path("pago", views.process_payment, name="process_payment"),
    path(
        "pago/order-details",
        views.checkout_order_details,
        name="checkout_order_details",
    ),
    path("pago/hecho", views.payment_done, name="payment_done"),
    path("pago/cancelado", views.payment_canceled, name="payment_cancelled"),
    path("pay-pal/create-order", views.paypal_create_order, name="paypal_create_order"),
    path("pay-pal/verify-order", views.paypal_verify_order, name="paypal_verify_order"),
    path(
        "pay-pal/capture-order", views.paypal_capture_order, name="paypal_capture_order"
    ),
    path(
        "orden/<int:pk>",
        views.OrderDetails.as_view(),
        name="order_details",
    ),
    path(
        "orden/<int:pk>/update-shipping-cost",
        views.update_order_shipping_cost,
        name="update-order-shipping-cost",
    ),
]

import math

wrapping_weight_per_piece = 200


def calculate_order_weight(items: list) -> int:
    total_weight = sum(
        ((item.product.weight + wrapping_weight_per_piece) * item.quantity)
        for item in items
    )
    # take the box in to account
    total_weight = total_weight + (total_weight * 0.50)
    kg = max(math.floor(total_weight / 1_000), 1)
    return kg

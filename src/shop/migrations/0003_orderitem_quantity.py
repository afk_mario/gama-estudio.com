# Generated by Django 4.1.5 on 2023-02-19 23:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("shop", "0002_alter_customer_email_alter_customer_name_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="orderitem",
            name="quantity",
            field=models.IntegerField(default=1),
        ),
    ]

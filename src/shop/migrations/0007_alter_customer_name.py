# Generated by Django 4.1.5 on 2023-02-20 17:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("shop", "0006_rename_date_ordered_order_date_created"),
    ]

    operations = [
        migrations.AlterField(
            model_name="customer",
            name="name",
            field=models.CharField(blank=True, default="", max_length=240, null=True),
        ),
    ]

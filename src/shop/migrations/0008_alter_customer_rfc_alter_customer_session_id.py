# Generated by Django 4.1.5 on 2023-02-20 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("shop", "0007_alter_customer_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="customer",
            name="rfc",
            field=models.CharField(
                blank=True, default="", max_length=240, null=True, verbose_name="RFC"
            ),
        ),
        migrations.AlterField(
            model_name="customer",
            name="session_id",
            field=models.CharField(max_length=240, null=True),
        ),
    ]

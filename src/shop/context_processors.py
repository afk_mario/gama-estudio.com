from .constants import PaymentStatus
from .models import Customer, Order

DEFAULT_ORDER = {
    "get_subtotal_cost": 0,
    "get_total_cost": 0,
    "get_shipping_cost": 0,
    "get_cart_items": 0,
}


def cart_info(request):
    if not request.session or not request.session.session_key:
        request.session.save()
    session_key = request.session.session_key

    try:
        if request.user.is_authenticated:
            customer = Customer.objects.get(user=request.user)
        else:
            customer = Customer.objects.get(session_id=session_key, user=None)
    except Customer.DoesNotExist:
        customer = None

    try:
        order = Order.objects.get(
            customer=customer,
            payment_status=PaymentStatus.UNPAID,
        )
    except Order.DoesNotExist:
        order = None

    context = {"cart_order": order}

    return context

from django.core.mail import mail_admins, mail_managers, send_mail
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from cms.models import Piece

from .constants import PaymentStatus
from .models import Customer


def customer_get_or_404(request):
    user = request.user
    session_key = request.session.session_key
    if user.is_authenticated:
        customer = get_object_or_404(Customer, user=user)
    else:
        customer = get_object_or_404(Customer, session_id=session_key)

    return customer


def customer_get_or_none(request):
    session_key = request.session.session_key
    try:
        customer = Customer.objects.get(session_id=session_key)
    except Customer.DoesNotExist:
        customer = None

    return customer


def customer_get_or_create(request):
    user = request.user
    session_key = request.session.session_key

    if user.is_authenticated:
        customer, created = Customer.objects.get_or_create(user=user)
        if created:
            customer.name = request.user.first_name
            if request.user.last_name:
                customer.name = f"{request.user.first_name} {request.user.last_name}"
            customer.session_id = session_key
            customer.email = request.user.email
            customer.save()
    else:
        customer, created = Customer.objects.get_or_create(
            session_id=session_key, user=None
        )
    return customer


def order_handle_success(order):
    order.payment_status = PaymentStatus.PAID
    order.save()

    shipping_address = order.shipping_address
    items = order.orderitem_set.filter(product__status="stck")

    context = {
        "order": order,
        "order_items": items,
        "shipping_address": shipping_address,
    }

    confirmation_text_body = render_to_string(
        "shop/mail/order-confirmation.txt", context
    )

    notification_text_body = render_to_string(
        "shop/mail/order-notification.txt", context
    )

    Piece.objects.filter(orderitem__in=items).update(status="sold")

    send_mail(
        subject=f"[Gama Estudio] Confirmación de orden: {order.id}",
        recipient_list=[order.customer.email],
        from_email="tienda@gama-estudio.com",
        message=confirmation_text_body,
        fail_silently=False,
    )

    mail_managers(
        subject=f"Nueva compra: {order.id}",
        message=notification_text_body,
        fail_silently=False,
    )

    mail_admins(
        subject=f"Nueva compra: {order.id}",
        message=notification_text_body,
        fail_silently=False,
    )

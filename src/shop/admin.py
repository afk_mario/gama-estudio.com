from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import (
    Customer,
    Order,
    OrderItem,
    ShippingAddress,
    ShippingCost,
    ShippingCostInfo,
)


class OrderItemInline(admin.TabularInline):
    verbose_name = "Order Item"
    model = OrderItem
    extra = 0
    can_delete = False
    readonly_fields = ("product_price", "product_weight")

    def product_price(self, obj):
        return obj.product.price_discounted if obj.product else "N/A"

    product_price.short_description = "Price (MXN)"  # Label in the admin panel

    def product_weight(self, obj):
        return obj.product.weight if obj.product else "N/A"

    product_weight.short_description = "Weight (kg)"

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class ShippingCostInline(admin.TabularInline):
    verbose_name = "Costo por kg"
    model = ShippingCost
    extra = 0
    can_delete = True


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "user",
        "session_id",
        "name",
        "phone_number",
        "email",
        "rfc",
    ]
    list_display_links = [
        "id",
        "user",
        "session_id",
        "name",
        "phone_number",
        "email",
        "rfc",
    ]


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_per_page = 20
    view_on_site = True
    date_hierarchy = "date_created"
    inlines = [OrderItemInline]
    readonly_fields = [
        "transaction_id",
        "get_subtotal_cost",
        "get_shipping_cost",
        "get_total_cost",
    ]
    list_filter = [
        "order_status",
        "payment_status",
    ]
    list_display = [
        "id",
        "customer",
        "date_created",
        "order_status",
        "payment_status",
        "transaction_id",
        "get_subtotal_cost",
        "get_shipping_cost",
        "get_total_cost",
        "order_items",
        "shipping_address",
    ]
    list_display_links = [
        "id",
        "date_created",
        "order_status",
        "payment_status",
        "transaction_id",
        "get_total_cost",
        "order_items",
        "shipping_address",
    ]

    @admin.display(description="Items")
    def order_items(self, obj):
        return ", ".join([k.product.name for k in obj.orderitem_set.all()])


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "order",
        "product",
    ]
    list_display_links = [
        "id",
        "order",
        "product",
    ]


@admin.register(ShippingCostInfo)
class ShippingCostInfoAdmin(SingletonModelAdmin):
    inlines = [ShippingCostInline]


@admin.register(ShippingAddress)
class ShippingAddressAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "customer_name",
        "postal_code",
        "address_01",
        "address_02",
        "state",
        "city",
    ]

    @admin.display(description="Customer")
    def customer_name(self, obj):
        if obj.customer:
            return obj.customer.name or "Anonymous"
        else:
            return None
